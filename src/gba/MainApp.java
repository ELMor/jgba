package gba;

import gba.debug.Logger;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class MainApp extends JDialog implements Observer {
	static final long serialVersionUID = 0x023248248;

	Logger log; // Componente que contiene los mensajes de log
	private JTable jTable1;
	private JButton jBKeyAssign;

	private JTabbedPane tabPan;

	private JPanel mainPanel;

	private JTextArea messageArea;
	private JPanel jPanel1;
	private JPanel jPanel2;
	private JLabel jLabelAssign;

	private JScrollPane logPanel;

	private JPanel confPanel;

	private JButton startEmulation;

	private JButton jfcButton;

	private JTextField romNameField;

	private JPanel buttonPanel;

	private GBAPanel gbaf;
	
	private String kMaps[][]=new String[][] { 
			{ "A", 		"82" }, 
			{ "B", 		"84" }, 
			{ "Select", "8" }, 
			{ "Start", 	"10" }, 
			{ "Right", 	"39" }, 
			{ "Left", 	"37" }, 
			{ "Up", 	"38" }, 
			{ "Down", 	"40" }, 
			{ "L1", 	"89" }, 
			{ "R1", 	"69" } 
			};

	public static void main(String[] args) {
		new MainApp(480,320);
	}

	public MainApp(int ancho, int alto) {
		setTitle("jGBA - The ELM Street");

		log = new Logger();
		log.addObserver(this);

		this.setVisible(true);
		this.setFocusTraversalKeysEnabled(false);
		{
			{
			}
			tabPan = new JTabbedPane();
			getContentPane().add(tabPan);
			tabPan.setPreferredSize(new java.awt.Dimension(0, 0));
			{
				mainPanel = new JPanel();
				BorderLayout mainPanelLayout = new BorderLayout();
				tabPan.addTab("Principal", null, mainPanel, null);
				mainPanel.setPreferredSize(new java.awt.Dimension(0, 0));
				mainPanel.setLayout(new BorderLayout());
				{
					buttonPanel = new JPanel();
					BorderLayout buttonPanelLayout = new BorderLayout();
					mainPanel.add(buttonPanel, BorderLayout.SOUTH);
					buttonPanel.setLayout(buttonPanelLayout);
					{
						romNameField = new JTextField();
						buttonPanel.add(romNameField, BorderLayout.CENTER);
						romNameField.setText("C:/Downloads/_GBA/Juegos");
					}
					{
						jfcButton = new JButton();
						buttonPanel.add(jfcButton, BorderLayout.WEST);
						jfcButton.setText("...");
						jfcButton.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								gbaf.pausarEmulacion=true;
								startEmulation.setText("continue");
								JFileChooser jfc = new JFileChooser(
										romNameField.getText());
								if (jfc.showOpenDialog(MainApp.this) == JFileChooser.APPROVE_OPTION) {
									gbaf.reset();
									romNameField.setText(jfc.getSelectedFile()
											.getAbsolutePath());
									startEmulation.setText("start");
								}
							}
						});
					}
					{
						startEmulation = new JButton();
						buttonPanel.add(startEmulation, BorderLayout.EAST);
						startEmulation.setText("start");
						startEmulation.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								if (startEmulation.getText().equals("start")) {
									startEmulation.setText("pause");
									gbaf.pausarEmulacion=false;
									gbaf.setRomFileName(romNameField.getText());
									new Thread(gbaf).start();
								} else if (startEmulation.getText().equals(
										"pause")) {
									gbaf.pausarEmulacion = true;
									startEmulation.setText("continue");
								} else if(startEmulation.getText().equals("continue")){
									gbaf.pausarEmulacion=false;
									startEmulation.setText("pause");
								}
							}
						});
					}
				}
				{
					gbaf = new GBAPanel(log);
					mainPanel.add(gbaf, BorderLayout.CENTER);
				}
			}
			{
				confPanel = new JPanel();
				BorderLayout confPanelLayout = new BorderLayout();
				tabPan.addTab("Configuracion", null, confPanel, null);
				confPanel.setLayout(new BorderLayout());
				confPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
				{
					jLabelAssign = new JLabel();
					confPanel.add(jLabelAssign, BorderLayout.NORTH);
					jLabelAssign.setText("Asignacion de Teclas");
				}
				{
					jBKeyAssign = new JButton();
					confPanel.add(jBKeyAssign, BorderLayout.SOUTH);
					jBKeyAssign.setText("Aplicar");
					jBKeyAssign.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							jBKeyAssignActionPerformed(evt);
						}
					});
				}
				{
					jPanel1 = new JPanel();
					confPanel.add(jPanel1, BorderLayout.CENTER);
					{
						TableModel jTable1Model = new DefaultTableModel(
							kMaps,
							new String[] { "Column 1", "Column 2" });
						jTable1 = new JTable();
						jPanel1.add(getJTable1());
						jTable1.setModel(jTable1Model);
						jTable1.setBounds(49, 14, 315, 210);
						jTable1.setPreferredSize(new java.awt.Dimension(147, 161));
						jTable1.addKeyListener(new KeyAdapter() {
							public void keyPressed(KeyEvent evt) {
								jTable1KeyPressed(evt);
							}
						});
					}
				}
			}
			{
				logPanel = new JScrollPane(messageArea = new JTextArea());
				tabPan.addTab("Mensajes", null, logPanel, null);
				{
					messageArea.setText("java GameBoy Advance Emulator\n (c) The ELM Street Company - 2006\n");
					messageArea.setWrapStyleWord(true);
				}
			}
		}
		this.setSize(460, 400);
		this.validate();
	}

	protected void processWindowEvent(WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == 201)
			System.exit(0);
	}

	public void update(Observable o, Object arg) {
		messageArea.setText(log.giveMeTheLast(128));
	}
	
	public JTable getJTable1() {
		return jTable1;
	}
	
	private void jBKeyAssignActionPerformed(ActionEvent evt) {
		int newTeclas[]=new int[10];
		boolean ok=true;
		for(int i=0;i<10;i++){
			String s=(String)jTable1.getValueAt(i, 1);
			try {
				newTeclas[i]=Integer.parseInt(s);
			}catch(Exception e){
				ok=false;
			}
		}
		if(ok)
			gbaf.setTeclas(newTeclas);
	}
	
	private void jTable1KeyPressed(KeyEvent evt) {
		int r=jTable1.getSelectedRow();
		if(r==-1)
			return;
		jTable1.setValueAt(""+evt.getKeyCode(), r, 1);
	}

}
