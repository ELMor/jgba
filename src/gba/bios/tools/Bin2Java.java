package gba.bios.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;

public class Bin2Java {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length!=2){
			System.out.println("binFile file.txt");
			return;
		}
		String binFileName=args[0];
		String fileName=args[1];
		try {
			FileInputStream fis=new FileInputStream(binFileName);
			FileWriter fw=new FileWriter(fileName);
			fw.write("public static char values[]={ \n");
			int charsPerLine=0,c;
			boolean firstChar=true;
			while( (c=fis.read())!=-1){
				fw.write(firstChar?" ":",");
				String hex=Integer.toHexString(c);
				while(hex.length()<2)
					hex="0"+hex;
				fw.write("0x"+hex);
				if(++charsPerLine==16){
					fw.write("\n");
					charsPerLine=0;
				}
				firstChar=false;
			}
			fw.write("};\n");
			fw.flush();
			fw.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
