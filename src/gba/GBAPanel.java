package gba;

import gba.cpu.CPU;
import gba.cpu.IRQManager;
import gba.cpu.StoppedCPUException;
import gba.debug.Dumper;
import gba.debug.Logger;
import gba.video.Video;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.DirectColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.ImageProducer;

public class GBAPanel extends Panel implements Runnable, ImageProducer {

	static final long serialVersionUID = 0x87484955;
	final static int gbaWidth=240;
	final static int gbaHeight=160;

	int srNumber = 0;
	int frameRate = 0;
	boolean necesitaRefresco;

	DirectColorModel dcm;
	Image img;
	ImageConsumer imgConsumer;

	public boolean pausarEmulacion = false;
	String romName = null;

	public CPU cpu=null;
	Logger	logger=null;
	public int videoMemory[];
	public boolean finEmulacion=false;

	private int teclas[]={82,84,8,10,39,37,38,40,89,69};
	private int revTeclas[]=new int[255];
	int keybStatus=0xFFFF;
	
	private Graphics graphics=null;

	public GBAPanel(Logger lo) {
		logger=lo;
		setTeclas(teclas);
	}
	
	public void reset(){
		if(cpu!=null){
			finEmulacion=true;
			Thread.yield();
			cpu.memoria=null;
			cpu=null;
		}
		videoMemory=null;
	}

	void unRefrescoCompleto() throws StoppedCPUException {
		boolean needDraw;
		if (frameRate > 0)
			needDraw = srNumber % (frameRate + 1) == 0;
		else
			needDraw = true;
		srNumber++;
		cpu.memoria.mIO[4] &= 0xFC; // DISPSTAT
		cpu.memoria.mIO[6] = 0x00; // VCOUNT
		for (int row = 0; row < gbaHeight;) {
			cpu.memoria.mIO[4] &= 0xFD;
			if (cpu.memoria.mIO[5] == cpu.memoria.mIO[6]) {
				cpu.memoria.mIO[4] |= 0x04;
				if ((cpu.memoria.mIO[4] & 0x20) != 0)
					IRQManager.interruptRequestFlagsAck(this, 2);
			} else {
				cpu.memoria.mIO[4] &= 0xFB;
			}
			if (needDraw)
				Video.draw(this);
			IRQManager.procIRQ(this);
			if (!cpu.VBlankIntrWait)
				if (cpu.thumbMode != 0)
					cpu.thumbEnv.execOp(960);
				else
					cpu.armEnv.execOp(960);
			cpu.memoria.mIO[4] |= 0x02;
			if ((cpu.memoria.mIO[4] & 0x10) != 0)
				IRQManager.interruptRequestFlagsAck(this, 1);
			cpu.memoria.dmaHandler(176, 1);
			cpu.memoria.dmaHandler(212, 1);
			IRQManager.procIRQ(this);
			if (!cpu.VBlankIntrWait)
				if (cpu.thumbMode != 0)
					cpu.thumbEnv.execOp(272);
				else
					cpu.armEnv.execOp(272);
			row++;
			cpu.memoria.mIO[6]++; // Increment Vertical Count
		}
		// Comprobamos las interrupciones
		cpu.memoria.mIO[4] &= 0xFD;
		cpu.memoria.mIO[4] |= 0x01;
		cpu.VBlankIntrWait = false;
		if ((cpu.memoria.mIO[4] & 8) != 0)
			IRQManager.interruptRequestFlagsAck(this, 0);
		cpu.memoria.dmaHandler(176, 0);
		cpu.memoria.dmaHandler(212, 0);
		// Comprobamos teclas
		loadBIOSKeyFlags();
		for (int l = 0; l < 68;) {
			if (cpu.memoria.mIO[5] == cpu.memoria.mIO[6]) {
				cpu.memoria.mIO[4] |= 0x04;
				if ((cpu.memoria.mIO[4] & 0x20) != 0)
					IRQManager.interruptRequestFlagsAck(this, 2);
			} else {
				cpu.memoria.mIO[4] &= '\373';
			}
			IRQManager.procIRQ(this);
			if (!cpu.VBlankIntrWait)
				if (cpu.thumbMode != 0)
					cpu.thumbEnv.execOp(0x4D1);
				else
					cpu.armEnv.execOp(0x4D1);
			l++;
			cpu.memoria.mIO[6]++;
		}
		cpu.memoria.mIO[4] &= 0xFE;
		// OK, volcamos el trabajo en el frame
		if (needDraw)
			syncDraw(videoMemory);
	}

	public synchronized boolean isConsumer(ImageConsumer imageconsumer) {
		return true;
	}

	public void setTeclas(int tec[]){
		if(tec.length!=10)
			return;
		teclas=tec;
		for(int i=0;i<revTeclas.length;i++)
			revTeclas[i]=0xFFFF;
		for(int i=0;i<10;i++)
			revTeclas[teclas[i]]= 0xFFFF & ((1 << i) ^ 0xFFFF);
	}

	void loadBIOSKeyFlags() {
		cpu.memoria.mIO[304] = (char) (keybStatus & 0xff);
		cpu.memoria.mIO[305] = (char) (keybStatus >> 8 & 0xff);
	}

	protected void processKeyEvent(KeyEvent keyevent) {
		switch(keyevent.getID()){
		default:break;
		case KeyEvent.KEY_PRESSED: 	keybStatus &= revTeclas[keyevent.getKeyCode()&0xFF]; break; 
		case KeyEvent.KEY_RELEASED: keybStatus |= 0xFFFF ^ revTeclas[keyevent.getKeyCode()&0xFF]; break;
		}
	}
	
	
	public void setRomFileName(String romFileName) {
		romName = romFileName;
	}

	public void run() {
		dcm = new DirectColorModel(32, 0xff0000, 0x00ff00, 0x0000ff, 0);
		img = Toolkit.getDefaultToolkit().createImage(this);
		cpu = new CPU(logger);
		videoMemory = new int[gbaWidth * gbaHeight];
		cpu.log.put("Empezando la emulacion "+romName);
		pausarEmulacion=false;
		finEmulacion=false;
		startEmulation(romName);
	}

	public void startEmulation(String romFileName) {
		int numPixels = gbaWidth * gbaHeight;
		enableEvents(8L);
		if (!RomLoader.loadROM(romFileName, this)) {
			cpu.log.put("No puedo cargar la ROM");
			return;
		}
		for (int k2 = 0; k2 < numPixels; k2++)
			videoMemory[k2] = 0;
		Video.copyFrame();
		long lastTime = System.currentTimeMillis();
		requestFocus();
		necesitaRefresco = false;
		int gbaFramesPerSecond = 0;
		long timeNow, fps = 0;
		try {
			do {
				if (finEmulacion) {
					return;
				}
				if( cpu.haltError ){
					dumpCPU("cpu Halt error:");
					return;
				}
				try {
					unRefrescoCompleto();
				} catch (RuntimeException e) {
					e.printStackTrace();
					dumpCPU(e.getMessage());
					return;
				}
				if (fps > 100)
					try {
						Thread.sleep((fps - 100) / 50);
					} catch (Exception e) {
					}
				timeNow = System.currentTimeMillis();
				fps = 0x186a0L / 60 / (timeNow - lastTime + 1);
				lastTime = timeNow;
				if (++gbaFramesPerSecond == 120) {
					gbaFramesPerSecond = 0;
					cpu.log.put("FPS=" + fps + " %");
				}
				if (necesitaRefresco)
					do
						syncDraw(videoMemory);
					while (necesitaRefresco);
				while (pausarEmulacion) {
					try {
						Thread.sleep(100);
					} catch (Exception e) {
					}
				}

			} while (true);
		} catch (StoppedCPUException sce) {
			cpu.log.put(sce.getMessage());
		}
	}

	private void dumpCPU(String str) {
		cpu.log.put("Oooops! "+str);
		Dumper.cpuStatus(cpu);
		cpu.log.setChanged();
	}

	public void startProduction(ImageConsumer imageconsumer) {
		addConsumer(imageconsumer);
	}

	public  void addConsumer(ImageConsumer iCons) {
		imgConsumer = iCons;
		imgConsumer.setDimensions(gbaWidth, gbaHeight);
		imgConsumer.setHints(30);
		imgConsumer.setColorModel(dcm);
	}

	public  void syncDraw(int[] pix) {
		if (imgConsumer != null) {
			imgConsumer.setPixels(0, 0, gbaWidth, gbaHeight, dcm, pix, 0, gbaWidth);
			imgConsumer.imageComplete(ImageConsumer.SINGLEFRAMEDONE);
		}
		//Mantenemos el aspect-ratio
		int w=getWidth();
		int h=getHeight();
		if(graphics==null)
			graphics=getGraphics();
		graphics.drawImage(img,
				0, 0, Math.min(w, h*3/2), Math.min(w*2/3, getHeight()), 
				0, 0, gbaWidth, gbaHeight, 
				null);
	}

	public  void removeConsumer(ImageConsumer imageconsumer) {
	}

	public void requestTopDownLeftRightResend(ImageConsumer imageconsumer) {
	}

}
