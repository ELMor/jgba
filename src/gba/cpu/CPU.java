package gba.cpu;

import gba.debug.Logger;
import gba.memoria.Memoria;

public final class CPU 
{

    public int reg[] = new int[16];
    public boolean VBlankIntrWait=false;
    public int pc;
    public int lRegs1[] = new int[32];
    public int lRegs2[] = new int[32];
    public int shRegs[] = new int[256];

    public int CPSR;
    public int save[] = new int[32];
    public int thumbMode;

    public Logger log=null;
    
    public ARMMode armEnv=null;
    public ThumbMode thumbEnv=null;
    public Memoria memoria=null;
    public boolean haltError=false;
    
    public CPU(Logger log){
    	memoria=new Memoria();
    	armEnv=new ARMMode(this);
    	thumbEnv=new ThumbMode(this);
    	this.log=log;
        for(int j = 0; j < 16; j++)
            reg[j] = 0;

        for(int k = 0; k < 32; k++)
            lRegs1[k] = 0;

        for(int l = 0; l < 32; l++)
            lRegs2[l] = 0;

        for(int i1 = 0; i1 < 32; i1++)
            save[i1] = 0;

        for(int j1 = 0; j1 < 256; j1++)
            shRegs[j1] = (1 << 32 - j1) - 1;

        CPSR = 211;
        thumbMode = 0;
        VBlankIntrWait = false;
        lRegs1[18] = 0x3007fa0;
        lRegs2[18] = 0;
        lRegs1[19] = 0x3007fe0;
        lRegs2[19] = 0;
        reg[13] = 0x3007fe0;
        pc = reg[15] = 0x8000000;
        log.trace(pc);
    }
    
    public void setNotSigned()
    {
        CPSR &= 0x7fffffff;
    }

    public void setNotZero()
    {
        CPSR &= 0xbfffffff;
    }

    public void setNotCarry()
    {
        CPSR &= 0xdfffffff;
    }

    public void setNotOverflow()
    {
        CPSR &= 0xefffffff;
    }

    public void enableIRQ()
    {
        CPSR &= 0xffffff7f;
    }

    public void enableFIQ()
    {
        CPSR &= 0xffffffbf;
    }

    public void setARMMode()
    {
        CPSR &= 0xffffffdf;
    }

    public void setSigned()
    {
        CPSR |= 0x80000000;
    }

    public void setZero()
    {
        CPSR |= 0x40000000;
    }

    public void setCarry()
    {
        CPSR |= 0x20000000;
    }

    public void setOverflow()
    {
        CPSR |= 0x10000000;
    }

    public void disableIRQ()
    {
        CPSR |= 0x80;
    }

    public void disableFIQ()
    {
        CPSR |= 0x40;
    }

    public void setThumbMode()
    {
        CPSR |= 0x20;
    }

    public void setSign(int j)
    {
        CPSR = CPSR & 0x7fffffff | j & 0x80000000;
    }

    public void setZero(int j)
    {
        if(j == 0)
            CPSR |= 0x40000000;
        else
            CPSR &= 0xbfffffff;
    }

    public void setCarry(int j)
    {
        CPSR = CPSR & 0x3fffffff | j & 0x80000000;
        if(j == 0)
            CPSR |= 0x40000000;
    }

    public void setOverflow(int j)
    {
        if(j != 0)
            CPSR |= 0x20000000;
        else
            CPSR &= 0xdfffffff;
    }

    public void setStatusBits02(int zero, int carry, int sign)
    {
        CPSR = CPSR & 0xfffffff | sign & 0x80000000;
        if(sign == 0)
            CPSR |= 0x40000000;
        if(((zero & carry | zero & ~sign | carry & ~sign) & 0x80000000) != 0)
            CPSR |= 0x20000000;
        if(((zero & carry & ~sign | ~zero & ~carry & sign) & 0x80000000) != 0)
            CPSR |= 0x10000000;
    }

    public void setStatusBits(int zero, int carry, int sign)
    {
        CPSR = CPSR & 0xfffffff | sign & 0x80000000;
        if(sign == 0)
            CPSR |= 0x40000000;
        if(((zero & ~carry | zero & ~sign | ~carry & ~sign) & 0x80000000) != 0)
            CPSR |= 0x20000000;
        if(((zero & ~carry & ~sign | ~zero & carry & sign) & 0x80000000) != 0)
            CPSR |= 0x10000000;
    }

    public int getSign()
    {
        return CPSR >> 31 & 1;
    }

    public int getZero()
    {
        return CPSR >> 30 & 1;
    }

    public int getCarry()
    {
        return CPSR >> 29 & 1;
    }

    public int getOverflow()
    {
        return CPSR >> 28 & 1;
    }

    public int getIRQDisabled()
    {
        return CPSR >> 7 & 1;
    }

    public int getFIQ()
    {
        return CPSR >> 6 & 1;
    }

    public int getARM()
    {
        return CPSR >> 5 & 1;
    }

    public int getControlBits()
    {
        return CPSR & 0x1f;
    }

}
