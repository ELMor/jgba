package gba.cpu;

public class StoppedCPUException extends Exception {
	public StoppedCPUException(String msg){
		super(msg);
	}
}
