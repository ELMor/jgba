package gba.cpu;
import gba.GBAPanel;

public final class IRQManager
{

    static boolean enabled = true;
    static int irqReqFlag = 0;
    static char vcountFlags[] = {
        '\001', '\002', '\004', '\b', '\020', ' ', '@', '\200', '\u0100', '\u0200', 
        '\u0400', '\u0800', '\u1000', '\u2000'
    };

    static void checkInterruptRequestFlag(GBAPanel gbaf)
    {
        int irqh = gbaf.cpu.memoria.mIO[0x202] | gbaf.cpu.memoria.mIO[0x203] << 8;
        if(irqh != irqReqFlag)
        {
            irqReqFlag &= irqh ^ 0xffff;
            gbaf.cpu.memoria.mIO[0x202] = (char)(irqReqFlag & 0xff);
            gbaf.cpu.memoria.mIO[0x203] = (char)(irqReqFlag >> 8 & 0xff);
        }
    }

    public static void procIRQ(GBAPanel gbaf)
    {
        checkInterruptRequestFlag(gbaf);
        if(enabled && 
           (gbaf.cpu.memoria.mIO[0x208] & 1) != 0 &&  //IRQ Master 
            gbaf.cpu.getIRQDisabled() == 0 && 
           ((gbaf.cpu.memoria.mIO[0x200] | gbaf.cpu.memoria.mIO[0x201] << 8) & irqReqFlag) != 0) //IRQ Enable
        {
            irqReqFlag &= irqReqFlag ^ 0xffff;
            gbaf.cpu.save[18] = gbaf.cpu.CPSR;
            int cpuCntBits = gbaf.cpu.getControlBits();
            gbaf.cpu.lRegs1[cpuCntBits] = gbaf.cpu.reg[13];
            gbaf.cpu.lRegs2[cpuCntBits] = gbaf.cpu.reg[14];
            gbaf.cpu.CPSR = gbaf.cpu.CPSR & 0xffe0 | 0x12;
            cpuCntBits = gbaf.cpu.getControlBits();
            gbaf.cpu.reg[13] = gbaf.cpu.lRegs1[cpuCntBits];
            gbaf.cpu.reg[14] = gbaf.cpu.lRegs2[cpuCntBits];
            gbaf.cpu.reg[13] -= 4;
            gbaf.cpu.memoria.writeWord(gbaf.cpu.reg[13], gbaf.cpu.pc + 4);
            gbaf.cpu.reg[13] -= 4;
            gbaf.cpu.memoria.writeWord(gbaf.cpu.reg[13], gbaf.cpu.reg[12]);
            gbaf.cpu.reg[13] -= 4;
            gbaf.cpu.memoria.writeWord(gbaf.cpu.reg[13], gbaf.cpu.reg[3]);
            gbaf.cpu.reg[13] -= 4;
            gbaf.cpu.memoria.writeWord(gbaf.cpu.reg[13], gbaf.cpu.reg[2]);
            gbaf.cpu.reg[13] -= 4;
            gbaf.cpu.memoria.writeWord(gbaf.cpu.reg[13], gbaf.cpu.reg[1]);
            gbaf.cpu.reg[13] -= 4;
            gbaf.cpu.memoria.writeWord(gbaf.cpu.reg[13], gbaf.cpu.reg[0]);
            gbaf.cpu.reg[0] = gbaf.cpu.pc = gbaf.cpu.memoria.readWord(0x3007ffc);
            gbaf.cpu.log.trace(gbaf.cpu.pc);
            
            gbaf.cpu.reg[14] = 44;
            gbaf.cpu.thumbMode = 0;
            gbaf.cpu.setARMMode();
            enabled = false;
        }
    }

    public static void interruptRequestFlagsAck(GBAPanel gbaf, int k)
    {
        checkInterruptRequestFlag(gbaf);
        irqReqFlag |= vcountFlags[k];
        gbaf.cpu.memoria.mIO[0x202] = (char)(irqReqFlag & 0xff);
        gbaf.cpu.memoria.mIO[0x203] = (char)(irqReqFlag >> 8 & 0xff);
    }

}
