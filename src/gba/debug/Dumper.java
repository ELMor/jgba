package gba.debug;
import gba.GBAPanel;
import gba.cpu.CPU;

public final class Dumper
{
	
	public static String format(int sz, int r){
		String hex=Integer.toHexString(r);
		for(int i=hex.length();i<sz;i++)
			hex="0"+hex;
		return hex;
	}
	
	public static void cpuStatus(GBAPanel gbaf){
		cpuStatus(gbaf.cpu);
	}
	
	public static void cpuStatus(CPU cpu)
    {
        cpu.log.put("R00=" + format(8,cpu.reg[0])+" R01=" + format(8,cpu.reg[1])+" R02=" + format(8,cpu.reg[2])+
        		" R03=" + format(8,cpu.reg[3])+" R04=" + format(8,cpu.reg[4])+" R05=" + format(8,cpu.reg[5])+
        		" R06=" + format(8,cpu.reg[6])+" R07=" + format(8,cpu.reg[7]));
        cpu.log.put("R08=" + format(8,cpu.reg[8])+" R09=" + format(8,cpu.reg[9])+" R10=" + format(8,cpu.reg[10])+
        		" R11=" + format(8,cpu.reg[11])+" R12=" + format(8,cpu.reg[12])+" SP =" + format(8,cpu.reg[13])+
        		" LR =" + format(8,cpu.reg[14])+" PC =" + format(8,cpu.pc));
        cpu.log.put("CPSR=" + format(8,cpu.CPSR));
        cpu.log.put("FLAGS=" + (cpu.getSign() != 1 ? "_" : "N") 
        		                    + (cpu.getZero() != 1 ? "_" : "Z") 
        		                    + (cpu.getOverflow() != 1 ? "_" : "V") 
        		                    + (cpu.getCarry() != 1 ? "_" : "C") 
        		                    + (cpu.getARM() != 1 ? "_" : "T") 
        		                    + (cpu.getIRQDisabled() != 1 ? "_" : "I") 
        		                    + (cpu.getFIQ() != 1 ? "_" : "F"));
        boolean thumb=cpu.thumbMode==0;
        for(int i=-8;i<8;i++){
        	int address=cpu.pc + (thumb?2*i:4*i);
        	int instruction=(thumb?
        			cpu.memoria.readHalfWord(address):
        			cpu.memoria.readWord(address));
        	String ret[]=
        		thumb ? 
        				Disassembler.decodeThumb(address, (short)instruction):
        				Disassembler.decodeArm(address, instruction);
        	String acu=format(8,address)+"\t"+format(4,instruction)+"\t";
        	for(int j=0;j<ret.length;j++)
        		acu+=ret[j]+"\t";
        	cpu.log.put(acu);
        }
    }
}
