package gba.debug;

public class Disassembler implements CoreConstants {
	/**
	 * Disassembler constructor comment.
	 */
	public Disassembler() {
		super();
	}

	public static String[] decodeArm(int address, int instruction) {

		// Create result array
		String[] result = null;

		// Decode instructions
		if ((instruction & INSTR_AIM_BX) == INSTR_AIT_BX)
			result = decodeArmBX(address, instruction);
		else if ((instruction & INSTR_AIM_B) == INSTR_AIT_B)
			result = decodeArmB(address, instruction);
		else if ((instruction & INSTR_AIM_MRS) == INSTR_AIT_MRS)
			result = decodeArmMRS(address, instruction);
		else if ((instruction & INSTR_AIM_MSR) == INSTR_AIT_MSR)
			result = decodeArmMSR(address, instruction);
		else if ((instruction & INSTR_AIM_MUL) == INSTR_AIT_MUL)
			result = decodeArmMUL(address, instruction);
		else if ((instruction & INSTR_AIM_LDR) == INSTR_AIT_LDR)
			result = decodeArmLDR(address, instruction);
		else if ((instruction & INSTR_AIM_LDRH) == INSTR_AIT_LDRH)
			result = decodeArmLDRH(address, instruction);
		else if ((instruction & INSTR_AIM_LDM) == INSTR_AIT_LDM)
			result = decodeArmLDM(address, instruction);
		else if ((instruction & INSTR_AIM_DATAPROCT) == INSTR_AIT_DATAPROCT)
			result = decodeArmDATAPROCT(address, instruction);
		else if ((instruction & INSTR_AIM_DATAPROC) == INSTR_AIT_DATAPROC)
			result = decodeArmDATAPROC(address, instruction);
		else {

			// Unknown instruction
			result = new String[2];
			result[0] = "<UNKNOWN>";
			result[1] = "";
		}

		// Retuen result
		return result;
	}

	private static String[] decodeArmB(int address, int instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		if ((instruction & INSTR_B_LINKBIT) != 0)
			result[0] = "BL" + getConditionCode(instruction);
		else
			result[0] = "B" + getConditionCode(instruction);

		// Set parameters
		result[1] = "#"
				+ toHexString(address
						+ (((instruction & INSTR_B_OFFSET) << 8) >> 6) + 8, 8);

		// Return result
		return result;
	}

	private static String[] decodeArmBX(int address, int instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "BX" + getConditionCode(instruction);

		// Set parameters
		result[1] = getRegisterCode(instruction & INSTR_MASK_R_00_03);

		// Return result
		return result;
	}

	private static String[] decodeArmDATAPROC(int address, int instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		switch (instruction & INSTR_DP_OP_MASK) {

		case INSTR_DP_OP_AND:
			result[0] = "AND" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_EOR:
			result[0] = "EOR" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_SUB:
			result[0] = "SUB" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_RSB:
			result[0] = "RSB" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_ADD:
			result[0] = "ADD" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_ADC:
			result[0] = "ADC" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_SBC:
			result[0] = "SBC" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_RSC:
			result[0] = "RSC" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_ORR:
			result[0] = "ORR" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_MOV:
			result[0] = "MOV" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_BIC:
			result[0] = "BIC" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_MVN:
			result[0] = "MVN" + getConditionCode(instruction);
			break;
		}

		// Add set flag
		if ((instruction & INSTR_DP_SETFLAGS) != 0)
			result[0] += "S";

		// Set parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15)
				+ ",";

		if ((instruction & INSTR_DP_OP_MASK) != INSTR_DP_OP_MOV
				&& (instruction & INSTR_DP_OP_MASK) != INSTR_DP_OP_MVN)
			result[1] += getRegisterCode((instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19)
					+ ",";

		// Decode second operand
		if ((instruction & INSTR_DP_IMMEDIATE) != 0) {

			result[1] += "#0x"
					+ Integer.toHexString(instruction & INSTR_DP_IMM_VALUE);

			if ((instruction & INSTR_DP_IMM_ROTATE) != 0)
				result[1] += " ROR #0x"
						+ Integer
								.toHexString((instruction & INSTR_DP_IMM_ROTATE) >> 8);

		} else {

			result[1] += getRegisterCode((instruction & INSTR_MASK_R_00_03) >> INSTR_SHFT_R_00_03);
			result[1] += decodeShift(instruction >> 4);

		}

		// Return result
		return result;
	}

	private static String[] decodeArmDATAPROCT(int address, int instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		switch (instruction & INSTR_DP_OP_MASK) {

		case INSTR_DP_OP_TST:
			result[0] = "TST" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_TEQ:
			result[0] = "TEQ" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_CMP:
			result[0] = "CMP" + getConditionCode(instruction);
			break;
		case INSTR_DP_OP_CMN:
			result[0] = "CMN" + getConditionCode(instruction);
			break;
		}

		// Set parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19)
				+ ",";

		// Decode second operand
		if ((instruction & INSTR_DP_IMMEDIATE) != 0) {

			result[1] += "#0x"
					+ Integer.toHexString(instruction & INSTR_DP_IMM_VALUE);

			if ((instruction & INSTR_DP_IMM_ROTATE) != 0)
				result[1] += " ROR #0x"
						+ Integer
								.toHexString((instruction & INSTR_DP_IMM_ROTATE) >> 8);

		} else {

			result[1] += getRegisterCode((instruction & INSTR_MASK_R_00_03) >> INSTR_SHFT_R_00_03);
			result[1] += decodeShift(instruction >> 4);

		}

		// Return result
		return result;
	}

	private static String[] decodeArmLDM(int address, int instruction) {

		// Create result
		String[] result = new String[2];

		int regIndex = (instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19;

		// Get flags
		boolean preIndexing = (instruction & INSTR_LDM_INDEXING) != 0;
		boolean upOffset = (instruction & INSTR_LDM_UP) != 0;
		boolean psrForce = (instruction & INSTR_LDM_PSRFORCE) != 0;
		boolean writeBack = (instruction & INSTR_LDM_WRITEBACK) != 0;
		boolean load = (instruction & INSTR_LDM_LOAD) != 0;
		boolean SP = regIndex == REG_SP;

		// Set opcode
		if (load)
			result[0] = "LDM" + getConditionCode(instruction);
		else
			result[0] = "STM" + getConditionCode(instruction);

		if (SP) {
			result[0] += (load ^ preIndexing) ? "F" : "E";
			result[0] += (load ^ upOffset) ? "A" : "D";
		} else {
			result[0] += upOffset ? "I" : "D";
			result[0] += preIndexing ? "B" : "A";
		}

		result[1] = getRegisterCode(regIndex);

		if (writeBack)
			result[1] += "!";

		result[1] += ",{";

		boolean addComma = false;
		int activeRegister = 1;

		// Write/load registers
		for (int i = 0; i < 16; i++) {

			// Check if register should be loaded/stored
			if ((instruction & activeRegister) != 0) {

				if (addComma)
					result[1] += ",";

				result[1] += getRegisterCode(i);
				addComma = true;
			}

			// Set next register bit
			activeRegister <<= 1;
		}

		result[1] += "}";

		if (psrForce)
			result[1] += "^";

		// Return result
		return result;
	}

	private static String[] decodeArmLDR(int address, int instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		if ((instruction & INSTR_LDR_LOAD) != 0)
			result[0] = "LDR" + getConditionCode(instruction);
		else
			result[0] = "STR" + getConditionCode(instruction);

		if ((instruction & INSTR_LDR_BYTE) != 0)
			result[0] += "B";
		if ((instruction & INSTR_LDR_WRITEBACK) != 0)
			result[0] += "T";

		// Set parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15)
				+ ",";
		result[1] += "["
				+ getRegisterCode((instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19);

		if ((instruction & INSTR_LDR_INDEXING) == 0)
			result[1] += "]";

		result[1] += ",";

		if ((instruction & INSTR_LDR_UP) == 0)
			result[1] += "-";

		if ((instruction & INSTR_LDR_IMMEDIATE) != 0) {

			result[1] += getRegisterCode(instruction & INSTR_MASK_R_00_03);
			result[1] += decodeShift(instruction >>> 4);
		} else
			result[1] += "#" + toHexString(instruction & INSTR_LDR_OFFSET, 0);

		if ((instruction & INSTR_LDR_INDEXING) != 0)
			result[1] += "]";

		if ((instruction & INSTR_LDR_WRITEBACK) == 0)
			result[1] += "!";

		// Return result
		return result;
	}

	private static String[] decodeArmLDRH(int address, int instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		if ((instruction & INSTR_LDR_LOAD) != 0)
			result[0] = "LDR" + getConditionCode(instruction);
		else
			result[0] = "STR" + getConditionCode(instruction);

		if ((instruction & INSTR_LDRH_SIGNED) != 0)
			result[0] += "S";
		if ((instruction & INSTR_LDRH_HALFWORD) != 0)
			result[0] += "H";
		if ((instruction & INSTR_LDRH_SIGNED) != 0
				&& (instruction & INSTR_LDRH_HALFWORD) == 0)
			result[0] += "B";

		// Set parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15)
				+ ",";
		result[1] += "["
				+ getRegisterCode((instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19);

		if ((instruction & INSTR_LDR_INDEXING) == 0)
			result[1] += "]";

		result[1] += ",";

		if ((instruction & INSTR_LDR_UP) == 0)
			result[1] += "-";

		// Immediate offset
		if ((instruction & INSTR_LDRH_IMMEDIATE) != 0) {

			// Get offset
			int offset = ((instruction & INSTR_LDRH_OFFSET_H) >> 8)
					| (instruction & INSTR_LDRH_OFFSET_L);

			// Add to paramters
			result[1] += "#" + toHexString(offset, 2);
		}

		// Register offset
		else
			result[1] += getRegisterCode(instruction & INSTR_MASK_R_00_03);

		if ((instruction & INSTR_LDR_INDEXING) != 0)
			result[1] += "]";

		if ((instruction & INSTR_LDR_WRITEBACK) == 0)
			result[1] += "!";

		// Return result
		return result;
	}

	private static String[] decodeArmMRS(int address, int instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "MRS" + getConditionCode(instruction);

		result[1] += getRegisterCode(instruction & INSTR_MASK_R_00_03) + ",";

		// Set parameters
		if ((instruction & INSTR_MRS_SPSR_FLAG) != 0)
			result[1] += "SPSR";
		else
			result[1] += "CPSR";

		// Return result
		return result;
	}

	private static String[] decodeArmMSR(int address, int instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "MSR" + getConditionCode(instruction);

		// Set parameters
		if ((instruction & INSTR_MSR_SPSR_FLAG) != 0)
			result[1] = "SPSR";
		else
			result[1] = "CPSR";

		if ((instruction & INSTR_MSR_TYPE_FLAG) == 0) {
			result[1] += "_flg";

			if ((instruction & INSTR_MSR_SRC_FLAG) != 0)
				result[1] += ",UNKNOWN";
			else
				result[1] += ","
						+ getRegisterCode(instruction & INSTR_MASK_R_00_03);
		} else {
			result[1] += ","
					+ getRegisterCode(instruction & INSTR_MASK_R_00_03);
		}

		// Return result
		return result;
	}

	private static String[] decodeArmMUL(int address, int instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "MUL" + getConditionCode(instruction);

		// Add condition code flag if needed
		if ((instruction & INSTR_MUL_COND_FLAG) != 0)
			result[0] += "S";

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19)
				+ ",";
		result[1] += getRegisterCode((instruction & INSTR_MASK_R_00_03)) + ",";
		result[1] += getRegisterCode((instruction & INSTR_MASK_R_08_11) >> INSTR_SHFT_R_08_11);

		// Add accumulation parameter if needed
		if ((instruction & INSTR_MUL_ACC_FLAG) != 0)
			result[1] += ","
					+ getRegisterCode((instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15);

		// Return result
		return result;
	}

	private static String decodeShift(int shift) {

		// Initialise result
		String result = "";

		// Get shift type
		int shiftType = shift & SHIFT_TYPE;

		// Immediate shift
		if ((shift & SHIFT_OP_MASK) == 0
				&& ((shift & SHIFT_AMOUNT_VALUE) >>> SHIFT_AMOUNT_SHIFT) == 0)
			return "";

		// Calculate shifts
		switch (shiftType) {

		case SHIFT_LSL:
			result = ",LSL ";
			break;
		case SHIFT_LSR:
			result = ",LSR ";
			break;
		case SHIFT_ASR:
			result = ",ASR ";
			break;
		case SHIFT_ROR:
			result = ",ROR ";
			break;
		}

		// Immediate shift
		if ((shift & SHIFT_OP_MASK) == 0) {

			if (((shift & SHIFT_AMOUNT_VALUE) >> SHIFT_AMOUNT_SHIFT) != 0)
				result += "#0x"
						+ Integer
								.toHexString((shift & SHIFT_AMOUNT_VALUE) >> SHIFT_AMOUNT_SHIFT);
		}

		// Register shift
		else
			result += getRegisterCode((shift & INSTR_MASK_R_08_11) >> INSTR_SHFT_R_08_11);

		// Return result
		return result;
	}

	public static String[] decodeThumb(int address, short instruction) {

		// Create result array
		String[] result = null;

		// Move shifted register (LSL)
		if ((instruction & INSTR_TIM_LSL) == INSTR_TIT_LSL)
			return decodeThumbLSL(address, instruction);

		// Move shifted register (LSR)
		if ((instruction & INSTR_TIM_LSR) == INSTR_TIT_LSR)
			return decodeThumbLSR(address, instruction);

		// Move shifted register (ASR)
		if ((instruction & INSTR_TIM_ASR) == INSTR_TIT_ASR)
			return decodeThumbASR(address, instruction);

		// Addition/substraction
		if ((instruction & INSTR_TIM_ADD) == INSTR_TIT_ADD)
			return decodeThumbADD(address, instruction);

		// Move/compare/add/substract immediate
		if ((instruction & INSTR_TIM_MOV) == INSTR_TIT_MOV)
			return decodeThumbMOV(address, instruction);

		// ALU operation
		if ((instruction & INSTR_TIM_ALU) == INSTR_TIT_ALU)
			return decodeThumbALU(address, instruction);

		// HIREG operation
		if ((instruction & INSTR_TIM_HIREG) == INSTR_TIT_HIREG)
			return decodeThumbHIREG(address, instruction);

		// PCLOAD operation
		if ((instruction & INSTR_TIM_PCLOAD) == INSTR_TIT_PCLOAD)
			return decodeThumbPCLOAD(address, instruction);

		// SPLOAD operation
		if ((instruction & INSTR_TIM_SPLOAD) == INSTR_TIT_SPLOAD)
			return decodeThumbSPLOAD(address, instruction);

		// LDR operation
		if ((instruction & INSTR_TIM_LDR) == INSTR_TIT_LDR)
			return decodeThumbLDR(address, instruction);

		// LDRHS operation
		if ((instruction & INSTR_TIM_LDRHS) == INSTR_TIT_LDRHS)
			return decodeThumbLDRHS(address, instruction);

		// LDRI operation
		if ((instruction & INSTR_TIM_LDRI) == INSTR_TIT_LDRI)
			return decodeThumbLDRI(address, instruction);

		// LDRH operation
		if ((instruction & INSTR_TIM_LDRH) == INSTR_TIT_LDRH)
			return decodeThumbLDRH(address, instruction);

		// LOADADDR operation
		if ((instruction & INSTR_TIM_LOADADDR) == INSTR_TIT_LOADADDR)
			return decodeThumbLOADADDR(address, instruction);

		// SPADD operation
		if ((instruction & INSTR_TIM_SPADD) == INSTR_TIT_SPADD)
			return decodeThumbSPADD(address, instruction);

		// PUSHPOP operation
		if ((instruction & INSTR_TIM_PUSHPOP) == INSTR_TIT_PUSHPOP)
			return decodeThumbPUSHPOP(address, instruction);

		// MULTLS operation
		if ((instruction & INSTR_TIM_MULTLS) == INSTR_TIT_MULTLS)
			return decodeThumbMULTLS(address, instruction);

		// SWI operation
		if ((instruction & INSTR_TIM_SWI) == INSTR_TIT_SWI)
			return decodeThumbSWI(address, instruction);

		// CBRANCH operation
		if ((instruction & INSTR_TIM_CBRANCH) == INSTR_TIT_CBRANCH)
			return decodeThumbCBRANCH(address, instruction);

		// BRANCH operation
		if ((instruction & INSTR_TIM_BRANCH) == INSTR_TIT_BRANCH)
			return decodeThumbBRANCH(address, instruction);

		// LBRANCH operation
		if ((instruction & INSTR_TIM_LBRANCH) == INSTR_TIT_LBRANCH)
			return decodeThumbLBRANCH(address, instruction);

		result = new String[2];
		result[0] = "<UNKNOWN>";

		// Retuen result
		return result;
	}

	private static String[] decodeThumbADD(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		if ((instruction & INSTR_ADD_OP) != 0)
			result[0] = "SUB";
		else
			result[0] = "ADD";

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_00_02)) + ",";
		result[1] += getRegisterCode((instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05)
				+ ",";

		if ((instruction & INSTR_ADD_IMMEDIATE) != 0)
			result[1] += toHexString(
					(instruction & INSTR_MASK_R_06_08) >> INSTR_SHFT_R_06_08, 2);
		else
			result[1] += getRegisterCode((instruction & INSTR_MASK_R_06_08) >> INSTR_SHFT_R_06_08);

		// Return result
		return result;
	}

	private static String[] decodeThumbALU(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		switch (instruction & INSTR_ALU_OP) {

		case INSTR_ALU_OP_AND:
			result[0] = "AND";
			break;
		case INSTR_ALU_OP_EOR:
			result[0] = "EOR";
			break;
		case INSTR_ALU_OP_LSL:
			result[0] = "LSL";
			break;
		case INSTR_ALU_OP_LSR:
			result[0] = "LSR";
			break;
		case INSTR_ALU_OP_ASR:
			result[0] = "ASR";
			break;
		case INSTR_ALU_OP_ADC:
			result[0] = "ADC";
			break;
		case INSTR_ALU_OP_SBC:
			result[0] = "SBC";
			break;
		case INSTR_ALU_OP_ROR:
			result[0] = "ROR";
			break;
		case INSTR_ALU_OP_TST:
			result[0] = "TST";
			break;
		case INSTR_ALU_OP_NEG:
			result[0] = "NEG";
			break;
		case INSTR_ALU_OP_CMP:
			result[0] = "CMP";
			break;
		case INSTR_ALU_OP_CMN:
			result[0] = "CMN";
			break;
		case INSTR_ALU_OP_ORR:
			result[0] = "ORR";
			break;
		case INSTR_ALU_OP_MUL:
			result[0] = "MUL";
			break;
		case INSTR_ALU_OP_BIC:
			result[0] = "BIC";
			break;
		case INSTR_ALU_OP_MVN:
			result[0] = "MVN";
			break;
		}

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_00_02)) + ",";
		result[1] += getRegisterCode((instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05);

		// Return result
		return result;
	}

	private static String[] decodeThumbASR(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "ASR";

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_00_02)) + ",";
		result[1] += getRegisterCode((instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05)
				+ ",";
		result[1] += (instruction & INSTR_LSL_OFFSET) >>> 6;

		// Return result
		return result;
	}

	private static String[] decodeThumbBRANCH(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "B";

		// Add parameters
		int offset = ((instruction & INSTR_BRANCH_OFFSET) << 5) >> 4;
		result[1] = toHexString(address + offset + 4, 8);

		// Return result
		return result;
	}

	private static String[] decodeThumbCBRANCH(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "B";

		// Fetch register Rn
		int offset = ((instruction & INSTR_CBRANCH_OFFSET) << 8) >> 7;
		int cond = (instruction & INSTR_CBRANCH_COND) >>> 8;

		// Sign extend offset
		if ((offset & 0x0100) != 0)
			offset |= 0xffffff00;

		// Add condition code
		result[0] += getConditionCode(cond << 28);

		result[1] = toHexString(address + offset + 4, 8);

		// Return result
		return result;
	}

	private static String[] decodeThumbHIREG(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		switch (instruction & INSTR_HIREG_OP) {

		case INSTR_HIREG_OP_ADD:
			result[0] = "ADD";
			break;
		case INSTR_HIREG_OP_CMP:
			result[0] = "CMP";
			break;
		case INSTR_HIREG_OP_MOV:
			result[0] = "MOV";
			break;
		case INSTR_HIREG_OP_BX:
			result[0] = "BX";
			break;
		}

		// Get source and destination registers
		int regRsIndex = ((instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05)
				+ ((instruction & INSTR_HIREG_OP_H2) != 0 ? 8 : 0);
		int regRdIndex = ((instruction & INSTR_MASK_R_00_02) >> INSTR_SHFT_R_00_02)
				+ ((instruction & INSTR_HIREG_OP_H1) != 0 ? 8 : 0);

		// Add parameters
		if ((instruction & INSTR_HIREG_OP) != INSTR_HIREG_OP_BX) {
			result[1] = getRegisterCode(regRdIndex) + ",";
			result[1] += getRegisterCode(regRsIndex);
		} else {
			result[1] = getRegisterCode(regRsIndex);
		}

		// Return result
		return result;
	}

	private static String[] decodeThumbLBRANCH(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "BL";

		if ((instruction & INSTR_LBRANCH_HI) != 0)
			result[1] = "HI ";
		else
			result[1] = "LO ";

		// Return result
		return result;
	}

	private static String[] decodeThumbLDR(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		if ((instruction & INSTR_TLDR_LOAD) != 0)
			result[0] = "LDR";
		else
			result[0] = "STR";

		// Add byte flag
		if ((instruction & INSTR_TLDR_BYTE) != 0)
			result[0] += "B";

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_00_02)) + ",";
		result[1] += "["
				+ getRegisterCode((instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05)
				+ ",";
		result[1] += getRegisterCode((instruction & INSTR_MASK_R_06_08) >> INSTR_SHFT_R_06_08)
				+ "]";

		// Return result
		return result;
	}

	private static String[] decodeThumbLDRH(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		if ((instruction & INSTR_TLDRH_LOAD) != 0)
			result[0] = "LDRH";
		else
			result[0] = "STRH";

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_00_02)) + ",";
		result[1] += "["
				+ getRegisterCode((instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05)
				+ ",";
		result[1] += toHexString(
				((instruction & INSTR_TLDRH_OFFSET) >>> 6) << 1, 2)
				+ "]";

		// Return result
		return result;
	}

	private static String[] decodeThumbLDRHS(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		boolean sign = (instruction & INSTR_LDRHS_SIGN) != 0;
		boolean halfword = (instruction & INSTR_LDRHS_HALFWORD) != 0;

		// Set opcode
		if (!sign && !halfword)
			result[0] = "STRH";
		else
			result[0] = "LDRH";

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_00_02)) + ",";
		result[1] += "["
				+ getRegisterCode((instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05)
				+ ",";
		result[1] += getRegisterCode((instruction & INSTR_MASK_R_06_08) >> INSTR_SHFT_R_06_08)
				+ "]";

		// Return result
		return result;
	}

	private static String[] decodeThumbLDRI(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		if ((instruction & INSTR_TLDR_LOAD) != 0)
			result[0] = "LDR";
		else
			result[0] = "STR";

		// Add byte flag
		int offset;
		if ((instruction & INSTR_TLDR_BYTE) != 0) {
			result[0] += "B";
			offset = (instruction & INSTR_LDRI_OFFSET) >>> 6;
		} else {
			offset = ((instruction & INSTR_LDRI_OFFSET) >>> 6) << 2;
		}

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_00_02)) + ",";
		result[1] += "["
				+ getRegisterCode((instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05)
				+ ",";
		result[1] += toHexString(offset, 2) + "]";

		// Return result
		return result;
	}

	private static String[] decodeThumbLOADADDR(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "ADD";

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_08_10) >> INSTR_SHFT_R_08_10)
				+ ",";

		if ((instruction & INSTR_LOADADDR_SP) != 0)
			result[1] += getRegisterCode(REG_SP) + ",";
		else
			result[1] += getRegisterCode(REG_PC) + ",";

		result[1] += toHexString((instruction & INSTR_LOADADDR_OFFSET) << 2, 2);

		// Return result
		return result;
	}

	private static String[] decodeThumbLSL(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "LSL";

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_00_02)) + ",";
		result[1] += getRegisterCode((instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05)
				+ ",";
		result[1] += (instruction & INSTR_LSL_OFFSET) >>> 6;

		// Return result
		return result;
	}

	private static String[] decodeThumbLSR(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "LSR";

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_00_02)) + ",";
		result[1] += getRegisterCode((instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05)
				+ ",";
		result[1] += (instruction & INSTR_LSL_OFFSET) >>> 6;

		// Return result
		return result;
	}

	private static String[] decodeThumbMOV(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		switch (instruction & INSTR_MOV_OP) {

		case INSTR_MOV_OP_MOV:
			result[0] = "MOV";
			break;
		case INSTR_MOV_OP_CMP:
			result[0] = "CMP";
			break;
		case INSTR_MOV_OP_ADD:
			result[0] = "ADD";
			break;
		case INSTR_MOV_OP_SUB:
			result[0] = "SUB";
			break;
		}

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_08_10) >> INSTR_SHFT_R_08_10)
				+ ",";
		result[1] += toHexString((instruction & INSTR_MOV_OFFSET), 2);

		// Return result
		return result;
	}

	private static String[] decodeThumbMULTLS(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Get flags
		boolean load = (instruction & INSTR_PUSHPOP_LOAD) != 0;

		if (load)
			result[0] = "LDMIA";
		else
			result[0] = "STMIA";

		result[1] = getRegisterCode((instruction & INSTR_MASK_R_08_10) >>> INSTR_SHFT_R_08_10)
				+ "!,";
		result[1] += "{";

		// Initialise active bit value
		boolean addComma = false;
		int activeRegister = 1;

		// Write/load registers
		for (int i = 0; i < 8; i++) {

			// Check if register should be loaded/stored
			if ((instruction & activeRegister) != 0) {

				if (addComma)
					result[1] += ",";

				// Get register index
				int registerIndex = i;

				// Add register code to result
				result[1] += getRegisterCode(registerIndex);
				addComma = true;
			}

			// Set next register bit
			activeRegister <<= 1;
		}

		result[1] += "}";

		// Return result
		return result;
	}

	private static String[] decodeThumbPCLOAD(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "LDR";

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_08_10) >> INSTR_SHFT_R_08_10)
				+ ",";
		result[1] += "[" + getRegisterCode(REG_PC) + ","
				+ toHexString((instruction & INSTR_PCLOAD_IMM) << 2, 2) + "]";

		// Return result
		return result;
	}

	private static String[] decodeThumbPUSHPOP(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Get flags
		boolean load = (instruction & INSTR_PUSHPOP_LOAD) != 0;

		if (load)
			result[0] = "POP";
		else
			result[0] = "PUSH";

		result[1] = "{";

		// Initialise active bit value
		boolean addComma = false;
		int activeRegister = 1;

		// Write/load registers
		for (int i = 0; i < 9; i++) {

			// Check if register should be loaded/stored
			if ((instruction & activeRegister) != 0) {

				if (addComma)
					result[1] += ",";

				// Get register index
				int registerIndex = i;

				// Adjust if pclr
				if (i == 8)
					registerIndex = load ? REG_PC : REG_LR;

				// Add register code to result
				result[1] += getRegisterCode(registerIndex);
				addComma = true;
			}

			// Set next register bit
			activeRegister <<= 1;
		}

		result[1] += "}";

		// Return result
		return result;
	}

	private static String[] decodeThumbSPADD(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		if ((instruction & INSTR_SPADD_SIGN) != 0)
			result[0] = "SUB";
		else
			result[0] = "ADD";

		// Add parameters
		result[1] = getRegisterCode(REG_SP) + ",";
		result[1] += toHexString((instruction & INSTR_SPADD_OFFSET) << 2, 2);

		// Return result
		return result;
	}

	private static String[] decodeThumbSPLOAD(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		if ((instruction & INSTR_SPLOAD_LOAD) != 0)
			result[0] = "LDR";
		else
			result[0] = "STR";

		// Add parameters
		result[1] = getRegisterCode((instruction & INSTR_MASK_R_08_10) >> INSTR_SHFT_R_08_10)
				+ ",";
		result[1] += "[" + getRegisterCode(REG_SP) + ","
				+ toHexString((instruction & INSTR_PCLOAD_IMM) << 2, 2) + "]";

		// Return result
		return result;
	}

	private static String[] decodeThumbSWI(int address, short instruction) {

		// Create result
		String[] result = new String[2];

		// Set opcode
		result[0] = "SWI";

		// Add parameters
		result[1] = toHexString(instruction & 0xff, 2);
		// Return result
		return result;
	}

	private static String getConditionCode(int instruction) {

		// Return instruction code
		switch (instruction & INSTR_MASK_COND) {

		case COND_EQ:
			return "EQ";
		case COND_NE:
			return "NE";
		case COND_CS:
			return "CS";
		case COND_CC:
			return "CC";
		case COND_MI:
			return "MI";
		case COND_PL:
			return "PL";
		case COND_VS:
			return "VS";
		case COND_VC:
			return "VC";
		case COND_HI:
			return "HI";
		case COND_LS:
			return "LS";
		case COND_GE:
			return "GE";
		case COND_LT:
			return "LT";
		case COND_GT:
			return "GT";
		case COND_LE:
			return "LE";
		case COND_AL:
			return "";
		}

		return null;
	}

	private static String getRegisterCode(int registerIndex) {

		if (registerIndex <= REG_R12)
			return "R" + Integer.toString(registerIndex);
		else if (registerIndex == REG_SP)
			return "SP";
		else if (registerIndex == REG_LR)
			return "LR";
		else if (registerIndex == REG_PC)
			return "PC";
		else if (registerIndex == REG_CPSR)
			return "CPSR";
		else if (registerIndex == REG_SPSR)
			return "SPSR";

		return null;
	}

	public static String toHexString(int value, int length) {

		String s = Integer.toHexString(value);

		while (s.length() < length)
			s = "0" + s;

		s = "0x" + s;

		return s;
	}
}
